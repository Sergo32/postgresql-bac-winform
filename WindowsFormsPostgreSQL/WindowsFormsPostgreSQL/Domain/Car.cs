﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsPostgreSQL.Domain
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime DateCur { get; set; }
    }
}
