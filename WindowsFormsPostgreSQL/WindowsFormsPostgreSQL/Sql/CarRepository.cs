﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsPostgreSQL.Domain;

namespace WindowsFormsPostgreSQL.Sql
{
    public class CarRepository : ICarRepository
    {   
        private readonly string _connectionString;

        public CarRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public void Delete(int id)
        {
            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(this._connectionString);
            npgSqlConnection.Open();
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"Delete From car where id={id}", npgSqlConnection);
            int count = npgSqlCommand.ExecuteNonQuery();
            npgSqlConnection.Close();
        }

        public int Insert(Car cs)
        {

            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(this._connectionString);
            npgSqlConnection.Open();
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"INSERT INTO car( name,date) VALUES ('{cs.Name??string.Empty}', '{cs.DateCur}'::timestamp)", npgSqlConnection);
            int count = npgSqlCommand.ExecuteNonQuery();
            npgSqlConnection.Close();
            if (count == 1)
            return 1;
            else return 0;
        }

        public IEnumerable<Car> Select()
        {
            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(this._connectionString);
            npgSqlConnection.Open();
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand("SELECT * FROM car", npgSqlConnection);
            NpgsqlDataReader npgSqlDataReader = npgSqlCommand.ExecuteReader();
            IEnumerable<Car> cars = null;
            List<Car> carsList = new List<Car>();
            Car car = new Car();
            if (npgSqlDataReader.HasRows)
            {

                foreach (DbDataRecord dbDataRecord in npgSqlDataReader)
                {
                    car = new Car
                    {
                        Id = (int)dbDataRecord["id"],
                        Name = dbDataRecord["name"].ToString()
                    };
                    car.DateCur = Convert.ToDateTime(dbDataRecord["date"].ToString());
                    carsList.Add(car);
                }

                cars = carsList;
            }
            else
            {
                return cars;
            }


            return cars;
        }

        public void Update(Car car)
        {
            NpgsqlConnection npgSqlConnection = new NpgsqlConnection(this._connectionString);
            npgSqlConnection.Open();
            NpgsqlCommand npgSqlCommand = new NpgsqlCommand($"UPDATE car SET name = '{car.Name}', date = '{car.DateCur}'::timestamp WHERE id = {car.Id}", npgSqlConnection);
            int count = npgSqlCommand.ExecuteNonQuery();
            npgSqlConnection.Close();
        }
    }
}
