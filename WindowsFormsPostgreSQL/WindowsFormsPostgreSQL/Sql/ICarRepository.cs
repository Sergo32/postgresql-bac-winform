﻿using System;
using System.Collections.Generic;
using System.Deployment.Application;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WindowsFormsPostgreSQL.Domain;

namespace WindowsFormsPostgreSQL.Sql
{
  public  interface ICarRepository
    {
        IEnumerable<Car> Select();
        int Insert(Car сar);
        void Update(Car сar);
        void Delete(int id);
    }
}
