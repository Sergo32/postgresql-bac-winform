﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsPostgreSQL.Domain;
using WindowsFormsPostgreSQL.Sql;

namespace WindowsFormsPostgreSQL
{
    public partial class FormPostgreSQL : Form
    {
        public string connectionString = "Server=localhost;Port=5432;User Id=postgres;Password=1234;Database=Car;";
        public CarRepository CarRepository;

        public FormPostgreSQL()
        {
            InitializeComponent();
        }

        private void FormPostgreSQL_Load(object sender, EventArgs e)
        {
            CarRepository = new CarRepository(connectionString);

        }

        private void textBoxId_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number))
            {
                e.Handled = true;
            }
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            Car car = new Car { Name = textBoxName.Text, DateCur = dateTimeCar.Value };
            int error = CarRepository.Insert(car);
            if (error == 1)
                MessageBox.Show("Запись добавлена");
            else
                MessageBox.Show("Что-то пошло не так");

        }

        private void buttonSelect_Click(object sender, EventArgs e)
        {

            List<Car> list = CarRepository.Select().ToList();

            dataGridViewCar.DataSource = list;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (textBoxId.Text==null)
                MessageBox.Show("Введите id");
          
            CarRepository.Delete(int.Parse(textBoxId.Text));
            List<Car> list = CarRepository.Select().ToList();
            dataGridViewCar.DataSource = list;
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (textBoxId.Text == null)
                MessageBox.Show("Введите id");

            Car car = new Car { Name = textBoxName.Text, DateCur = dateTimeCar.Value ,Id=int.Parse(textBoxId.Text) };
            CarRepository.Update(car);
            List<Car> list = CarRepository.Select().ToList();
            dataGridViewCar.DataSource = list;
        }
    }
}
